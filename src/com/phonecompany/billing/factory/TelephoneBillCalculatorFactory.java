package com.phonecompany.billing.factory;

import java.util.EnumMap;
import java.util.Map;

import com.phonecompany.billing.TelephoneBillCalculator;
import com.phonecompany.billing.enums.Type;
import com.phonecompany.billing.impl.TelephoneBillCalculatorImpl;

public final class TelephoneBillCalculatorFactory {
	private static final Map<Type, TelephoneBillCalculator> CAL_FUNC;
	
	static {
		CAL_FUNC = new EnumMap<Type, TelephoneBillCalculator>(Type.class);
		CAL_FUNC.put(Type.FIRST_TYPE, new TelephoneBillCalculatorImpl());
	}
	
	public static TelephoneBillCalculator getCalculatorByType(Type type) {
		return type == null ? CAL_FUNC.get(Type.FIRST_TYPE) : CAL_FUNC.get(type);
	}
}
