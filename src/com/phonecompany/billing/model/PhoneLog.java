package com.phonecompany.billing.model;

import java.time.LocalDateTime;

public class PhoneLog {
	private String phone;
	private LocalDateTime timeStart;
	private LocalDateTime timeEnd;
	
	public PhoneLog() {
		
	}
	
	public PhoneLog(String phone, LocalDateTime timeStart, LocalDateTime timeEnd) {
		this.phone = phone;
		this.timeStart = timeStart;
		this.timeEnd = timeEnd;
	}

	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public LocalDateTime getTimeStart() {
		return timeStart;
	}
	
	public void setTimeStart(LocalDateTime timeStart) {
		this.timeStart = timeStart;
	}
	
	public LocalDateTime getTimeEnd() {
		return timeEnd;
	}
	
	public void setTimeEnd(LocalDateTime timeEnd) {
		this.timeEnd = timeEnd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((timeEnd == null) ? 0 : timeEnd.hashCode());
		result = prime * result + ((timeStart == null) ? 0 : timeStart.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PhoneLog other = (PhoneLog) obj;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (timeEnd == null) {
			if (other.timeEnd != null)
				return false;
		} else if (!timeEnd.equals(other.timeEnd))
			return false;
		if (timeStart == null) {
			if (other.timeStart != null)
				return false;
		} else if (!timeStart.equals(other.timeStart))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PhoneLog [phone=" + phone + ", timeStart=" + timeStart + ", timeEnd=" + timeEnd + "]";
	}
}
