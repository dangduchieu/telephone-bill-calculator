package com.phonecompany.billing.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import com.phonecompany.billing.model.PhoneLog;

public final class CsvReader {

	private static final String TIME_FORMAT_PATTERN = "dd-MM-yyyy HH:mm:ss";
	private static final String BREAK_LINE = System.lineSeparator();
	private static final String COLUMN_SEPARATOR = ",";
	private static final String PHONE_NUMBER_REGEX = "[0-9]+";

	private CsvReader() {
		
	}

	public static List<PhoneLog> readPhoneLogs(String csv) {
		if (shouldNotReadLog(csv)) {
			return Collections.emptyList();
		}

		List<PhoneLog> phoneLogs = new LinkedList<>();
		for (String row : csv.split(BREAK_LINE)) {
			String[] data = row.split(COLUMN_SEPARATOR);
			if (validRow(data)) {
				phoneLogs.add(new PhoneLog(data[0], parseDateTime(data[1]), parseDateTime(data[2])));
			}
		}
		
		return phoneLogs;
	}

	private static boolean shouldNotReadLog(String csv) {
		return Objects.isNull(csv) || csv.trim().isEmpty();
	}

	private static LocalDateTime parseDateTime(String dateTimeStr) {
		return LocalDateTime.parse(dateTimeStr, DateTimeFormatter.ofPattern(TIME_FORMAT_PATTERN));
	}

	private static boolean validRow(String[] data) {
		if (data.length != 3) {
			return false;
		}

		String phoneNumber = data[0];
		String startTimeStr = data[1];
		String endTimeStr = data[2];

		return isValidPhoneNumber(phoneNumber) && validDateTime(startTimeStr) && validDateTime(endTimeStr);
	}

	private static boolean isValidPhoneNumber(String phoneNumber) {
		return Objects.nonNull(phoneNumber) && phoneNumber.matches(PHONE_NUMBER_REGEX);
	}

	private static boolean validDateTime(String dataTimeStr) {
		try {
			DateTimeFormatter.ofPattern(TIME_FORMAT_PATTERN).parse(dataTimeStr);
			return true;
		} catch (DateTimeParseException e) {
			return false;
		}
	}

}
